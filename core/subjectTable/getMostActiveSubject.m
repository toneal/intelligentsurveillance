function [ rv numFramesActive ] = getMostActiveSubject( subjectTable )

mostActive = [];
mostActiveCount = 0;

for i = 1:length(subjectTable)
    frameInfo = subjectTable{i}.frameInfo;
    frameCount = 0;
    for j = 1:length(frameInfo);
        if (~isempty(frameInfo{j}))
            frameCount = frameCount + 1;
        end
    end

    if (frameCount > mostActiveCount)
        mostActive = i;
        mostActiveCount = frameCount;
    end

end

rv = mostActive;
numFramesActive = mostActiveCount;

end

