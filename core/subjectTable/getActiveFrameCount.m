function rv = getActiveFrameCount(subjectTable, subjectId)
    frameInfo = subjectTable{subjectId}.frameInfo;
    rv = 0;
    for i = 1:length(frameInfo)
        if (~isempty(frameInfo{i}))
            rv = rv + 1;
        end
    end

end