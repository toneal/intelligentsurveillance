function bgSubtracted = bgsub_frame( frame, bgFrame )
%BGSUB_FRAME Subtracts out the background of a frame and applies some
%blurring


    % the scaling is still weird. Some negative numbers appear?
    bgsubtractCurr = sqrt(sum((frame - bgFrame).^2, 3));

    gauss = fspecial('gaussian', [20 20], 10);
    bgblurCurr = imfilter(bgsubtractCurr, gauss);
    
    % return the result
    bgSubtracted = bgblurCurr;
    
    imwrite(bgSubtracted./255,'../outputs/bgSubtracted0.png','png'); 

end

