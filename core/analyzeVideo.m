function retVal = analyzeVideo( vidData, ruleList )
%ANALYZEVIDEO   Takes in video data, a list of rules,
%and generates a data structure will all sorts of useful info, including
%which rules were violated and when



nFrames = size(vidData, 4);


bgFrame = double(rgb2gray(imread('../Datasets/calib_bg.png')));

global globalCache;
%subjectTable = analyzeMovingSubjects(vidData, bgFrame);
%globalCache.subjectTable = subjectTable;
subjectTable = globalCache.subjectTable; % hack to avoid recomputing for testing.  TODO: remove in final product

retVal = struct();

retVal.subjects = subjectTable;

nSubjects = length(subjectTable);

% perform rule violation analysis
% set up detection states associate with each rule trigger
% the first dimension of detectionStates is the rule trigger index
% the second dimension is the subject id
detectionStates = cell(length(ruleList), nSubjects);

% initialize the detection states
for i = 1:length(ruleList)
    for j = 1:nSubjects
        detState = struct();
        % each trigger type has a different detection state format
        trigger = ruleList{i}.trigger;
        if (strcmp(trigger.type, 'RestrictedRegionViolation'))
            % initialization for restricted region violation state
            detState.isInRegion = false;
            detState.frameEnteredRegion = -1;
            detState.triggered = false;
        elseif (strcmp(trigger.type, 'StillnessViolation'))
            detState.stillSince = -1;
            detState.still = false;
            detState.triggered = false;
        end
        detectionStates(i, j) = {detState};
    end
end

% initialize a place to put a record of each trigger that was activated
activatedTriggers = {};
eventCount = 0;

for i = 1:nFrames
    % iterate over each rule
    for j = 1:length(ruleList)
        rule = ruleList{j};
        trigger =rule.trigger;
        
        % iterate over each subject
        for k = 1:nSubjects
            subjectId = k;
            subject = subjectTable{k};
            subjFrameDatum = subject.frameInfo{i};
            % do processing for each rule type
            if (strcmp(trigger.type, 'RestrictedRegionViolation'))
                fprintf('RestrictedRegion proc\n');
                % load up the associated detection state
                detState = detectionStates{j, k};

               
                if (isempty(subjFrameDatum))
                    fprintf('frame data was empty for frame %d, subject %d, skipping\n', i, k);
                else
                     % determine if the subject is in the restricted region for
                     % this frame
                    subjBBox = subjFrameDatum.blob.BBox
                    ROI = trigger.regionOfInterest
                    subjInRestrRegionNow = test2DRectangleCollide(subjBBox, ROI)
                    
                    if (detState.isInRegion == false)
                        if (subjInRestrRegionNow)
                            % subject has just entered the region, set the
                            % start frame
                            detState.frameEnteredRegion = i;
                            detState.isInRegion = true;
                        end
                    else
                        % subject was in region last frame
                        timeDiff = i - detState.frameEnteredRegion;
                        if (timeDiff > trigger.minTime && ~detState.triggered)
                           fprintf('rule triggered, bitches! frame %d, subject %d\n', i, k);
                            % record activation of trigger
                            activatedTriggers = [activatedTriggers {struct('ruleIndex', j, 'subjectId', k, 'frame', i)}];
                            detState.triggered = true;
                            eventCount = eventCount + 1;
                        end
                        if (~subjInRestrRegionNow)
                            % subject has just left the region
                            detState.isInRegion = false;
                            detState.triggered = false;
                            activatedTriggers{eventCount}.('eventEnd') = i;
                        end
                    end
                    
                end
                


                % write back the detection state
                detectionStates{j,k} = detState;
            elseif (strcmp(trigger.type, 'StillnessViolation'))
                fprintf('Stillness proc\n');
                % load up the associated detection state
                detState = detectionStates{j, k};

               % skip first frame cause we can't get velocity infoz
                if ( i==1 || isempty(subjFrameDatum) || isempty(subject.frameInfo{i-1}))
                    fprintf('skipping %i, %i\n', i, k);
                else
                     % determine if the subject is still
                    % get velocity for current frame
                    posDiff = subjFrameDatum.blob.Centroid - subject.frameInfo{i-1}.blob.Centroid;
                    posDiffMag = norm(posDiff) * 10;
                    if (posDiffMag<trigger.stillnessThreshold)
                        if (~detState.still)
                            detState.stillSince = i;
                            detState.still = true;
                        else
                            % if was already still
                            if (i - detState.stillSince > trigger.stillnessTime && detState.triggered==false)
                                % stillness violation! record it and move
                                % on
                                activatedTriggers = [activatedTriggers {struct('ruleIndex', j, 'subjectId', k, 'frame', i)}];
                                detState.triggered = true;
                            end
                        end
                    else
                        % if movement detected
                        if (detState.still)
                            detState.still = false;
                            detState.triggered = false;
                        end
                        
                    end
                            
                            
                    
                    
                end
                


                % write back the detection state
                detectionStates{j,k} = detState;
            else
                error('unknown rule type %s', trigger.type);
            end
        
        end
        
        
    end
end % end iterating over frames

% TODO: add conditional processing here to filter out some of the activated
% triggers

retVal.activatedTriggers = activatedTriggers;


end
