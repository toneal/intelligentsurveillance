function rv = test2DRectangleCollide(rect1, rect2)
    rv = (rect1(1) + rect1(3) > rect2(1)) && (rect2(1) + rect2(3) > rect1(1)) ...
        && (rect1(2) + rect1(4) > rect2(2)) && (rect2(2) + rect2(4) > rect1(2));
end