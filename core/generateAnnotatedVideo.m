function outVid = generateAnnotatedVideo(vidWithContext, analysisResults, nFrames, roi)

% Unwrap data structure some. I wonder about the efficiency of passing it
% like this.
viddata = vidWithContext.rawData;


% Copied out of deliverable_multsubj. Seemed to do most of what we need
% right away.
outVid = viddata(:, :, :, 1:nFrames);
for i = 1:nFrames
    img = viddata(:, :, :, i);
    imgRaw = img;
    img = drawTrackingInfoOnImage(img, analysisResults, i, roi);
    outVid(:, : ,:, i) = img;
    fprintf('Rendered frame %d/%d\n', i, nFrames);
end

end