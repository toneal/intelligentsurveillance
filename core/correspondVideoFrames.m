function [newSubjectFrameInfos numNewSubjectsFound] = correspondVideoFrames( prevFrame, prevFrameIndex, frame, frameIndex, bgFrame, subjectTableSoFar, nextSubjectId )
% returns a cell array of SubjectFrameInfo objects representing the objects determined in the current frame 
% also returns the count of new, unrecognized subjects found in the current
% frame

prevFrameBlobs = analyzeVideoFrameGeometry(prevFrame, bgFrame);
currFrameBlobs = analyzeVideoFrameGeometry(frame, bgFrame);

numBlobsThisFrame = length(currFrameBlobs)

corrTable = blobCorrespondence(prevFrameBlobs, currFrameBlobs);

% takes in a correspondence table and updates a table of subject
% information accordingly.

% Build subject table of structs
    % each struct has:
    % id
    % centroid
    % bbox
    % is in restricted area


   listCurr = currFrameBlobs;
   

subjectFrameInfoRV = {};
numNewSubjectsFound = 0;

for ii = 1:length(listCurr) % iterate through new blob ids    
    currBlob = listCurr{ii};
    % check against correlation table
    prevBlobIndex = find(corrTable(:, ii) == 1);

    % see if the current blob corresponded to a blob in the previous frame
    if (~isempty(prevBlobIndex))
        % it did correspond to a blob in the previous frame
        % determine the subject ID of that blob
        subjID = findSubjectIDByBlobIndex(subjectTableSoFar, prevFrameIndex, prevBlobIndex);

        restrictAlert = 1; % Placeholder till I figure out restricted area stuff.
        subjectFrameInfo = struct('blob', currBlob, 'isInRestricted', restrictAlert, 'blobIndex', ii, 'subjID', subjID);
        
    else
        % it didn't correspond
        fprintf('It didn''t correspond, yo\n');
        % need to generate a new id
        newId = nextSubjectId;
        numNewSubjectsFound = numNewSubjectsFound + 1
        nextSubjectId = nextSubjectId + 1
        subjectFrameInfo = struct('blob', currBlob, 'isInRestricted', false, 'blobIndex', ii, 'subjID', newId);

    end

    % add the subjectframeinfo to the return value
    subjectFrameInfoRV = [subjectFrameInfoRV {subjectFrameInfo}];
  
   
end

newSubjectFrameInfos = subjectFrameInfoRV;

end

