function [ blobCorres ] = blobCorrespondence( listM, listN )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%   Constructing the cost matrix

distMat = zeros(length(listM), length(listN));
for rr=1:length(listM)
   for cc=1:length(listN)
      point1 = listM{rr}.Centroid;
      point2 = listN{cc}.Centroid;
      X = [point1(1,1), point1(1,2); point2(1,1), point2(1,2)];
      d = pdist(X,'euclidean');
      distMat(rr,cc) = d;
   end    
end

[blobCorres, cost] = munkres(distMat);

end

