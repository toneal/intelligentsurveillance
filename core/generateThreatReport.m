function output = generateThreatReport( videoData, threatAnalysisResult, ruleList )
% videoData contains the following info
% cameraId: identifying string for the camera we're using
% framesPerSecond: self-explanatory
% timestampStart: timestamp, in matlab's datenum format, for the "real-world" start of the
% video footage
% rawData: the 4-D vector of actual video footage

% util function for getting a date string given a video frame number
frmToDate = @(frmIndex) (datestr(videoData.timestampStart + frmIndex / videoData.framesPerSecond / 60 / 60 / 24));
b64ToImgTag = @(b64) ( ['<img src="data:image/png;base64,' b64 '" />']);

nFrames = size(videoData.rawData, 4);
activatedTriggers = threatAnalysisResult.activatedTriggers;

prIcons = struct();
prIcons.Alert = b64ToImgTag(generateBase64Image(imread('symbol-alert.png')));
prIcons.Warning = b64ToImgTag(generateBase64Image(imread('symbol-warning.png')));
prIcons.Log = b64ToImgTag(generateBase64Image(imread('symbol-log.png')));


tblHdStr = '<td class="table-header-cell">';

op = '';
op = [op '<html>'];
op = [op '<head>'];

op = [op '<style>'];

% embed the stylesheet in here
ssfh = fopen('style.css','rb');
ssd = fread(ssfh);
fclose(ssfh);
ssstr = char(ssd)';
op = [op ssstr];

op = [op '</style>'];


op = [op '<title>'];
op = [op '</title>'];
op = [op '</head>'];
op = [op '<body>'];

op = [op '<div class="masterDiv">'];

op = [op '<h1>'];
op = [op 'Threat Report: Camera ' videoData.cameraId];
op = [op '</h1>'];

op = [op '<h5>'];
op = [op 'Video Start: ' frmToDate(1)];
op = [op '</h5>'];

op = [op '<h5>'];
op = [op 'Video End: ' frmToDate(nFrames)];
op = [op '</h5>'];

op = [op '<h2>'];
op = [op 'All Triggered Rules'];
op = [op '</h2>'];
op = [op '<table>'];
    op = [op '<tr>'];
        op = [op tblHdStr];
            op = [op 'Time'];
         op = [op '</td>'];
         op = [op tblHdStr];
            op = [op 'Priority'];
         op = [op '</td>'];
         op = [op tblHdStr];
            op = [op 'Rule'];
         op = [op '</td>'];
         op = [op tblHdStr];
            op = [op 'Subject'];
         op = [op '</td>'];
         op = [op tblHdStr];
            op = [op 'Footage'];
         op = [op '</td>'];
    op = [op '</tr>'];
    
    % display list of violations in chronological order
    for i = 1:length(activatedTriggers)
        aTrigger = activatedTriggers{i};
        rule = ruleList{aTrigger.ruleIndex};
        frame = aTrigger.frame;
        subject = threatAnalysisResult.subjects{aTrigger.subjectId};
        subjectFrameDatum = subject.frameInfo{frame};
        op = [op '<tr>'];
             op = [op '<td>'];
             % get date str for this frame
             timestr = frmToDate(aTrigger.frame);
            op = [op timestr ' (Frame ' num2str(frame) ')'];
         op = [op '</td>'];
         op = [op '<td>'];
            op = [op prIcons.(rule.priority) ' ' rule.priority];
         op = [op '</td>'];
         op = [op '<td>'];
            op = [op rule.name];
         op = [op '</td>'];
         op = [op '<td>'];
            op = [op 'Subject ' num2str(aTrigger.subjectId)];
         op = [op '</td>'];
         op = [op '<td>'];
            % get corresponding frame
            frm = videoData.rawData(:, :, :, aTrigger.frame);
            
            scaleFactor = 120/size(frm, 1);
            
            % resize frame to a readable size, 120 by X
            frm = imresize(frm, scaleFactor);
            
             % draw any annotating data that should be included on the frame
            frm = generateThreatReport_annotateFrameSnapshot(frm, scaleFactor, rule, subjectFrameDatum);
            
            b64Frm = generateBase64Image(frm);
            op = [op b64ToImgTag(b64Frm)];
         op = [op '</td>'];
        op = [op '</tr>'];
    end
    
op = [op '</table>'];

op = [op '<h2>'];
op = [op 'All Subjects'];
op = [op '</h2>'];

op = [op '<table>'];
    op = [op '<tr>'];
        op = [op tblHdStr];
            op = [op 'ID'];
         op = [op '</td>'];
         op = [op tblHdStr];
            op = [op 'Entered'];
         op = [op '</td>'];
         op = [op tblHdStr];
            op = [op 'Exited'];
         op = [op '</td>'];
         op = [op tblHdStr];
            op = [op 'Position'];
         op = [op '</td>'];
        
    op = [op '</tr>'];
    
    % display list of subjects
    for i = 1:length(threatAnalysisResult.subjects)
        subject = threatAnalysisResult.subjects{i};
        op = [op '<tr>'];
             op = [op '<td>'];
                op = [op num2str(subject.id)];
            op = [op '</td>'];
            op = [op '<td>'];
                op = [op frmToDate(subject.frameEntered) ' (Frame ' num2str(subject.frameEntered) ')'];
            op = [op '</td>'];
            op = [op '<td>'];
                op = [op frmToDate(subject.frameExited) ' (Frame ' num2str(subject.frameExited) ')'];
            op = [op '</td>'];
            
            op = [op '<td>'];
                % get plot of kinematic data
                outPlot = subjPosTrajPlot(i, threatAnalysisResult.subjects);
                img0 = outPlot{1};
                % resize for readability, again 120px
                % todo: resize this better so we're not scaling down a huge
                % image
                scFact = 120/size(img0,1);
                img0 = imresize(img0, scFact);
                op = [op b64ToImgTag(generateBase64Image(img0))];
            op = [op '</td>'];
            
     
        op = [op '</tr>'];
    end
    
op = [op '</table>'];

op = [op '<hr />'];

op = [op '<p>'];
op = [op 'Report generated ' datestr(now) ' by ThreatSoft Security Assessment Software'];
op = [op '</p>'];

op = [op '</div>'];

op = [op '</body>'];

op = [op '</html>'];


output = op;

end

function frame = generateThreatReport_annotateFrameSnapshot(shrunkenFrame, scaleFactor, rule, subjectFrameDatum)
    frame = shrunkenFrame;
    trigger = rule.trigger;
    % draw subject bounding box
        subjBBox = subjectFrameDatum.blob.BBox;
        SBBsc = subjBBox * scaleFactor;
        frame = drawRect(SBBsc(2),SBBsc(1),SBBsc(4),SBBsc(3),frame,[255 255 0]);
    if (strcmp(trigger.type, 'RestrictedRegionViolation'))
        ROI = trigger.regionOfInterest;
        ROIsc = ROI * scaleFactor;
        frame = drawRect(ROIsc(2),ROIsc(1),ROIsc(4),ROIsc(3),frame,[255 0 0]);
    elseif (strcmp(trigger.type, 'StillnessViolation'))
        
        % do nothing special
    else
        error('Unknown trigger type');
    end
end

