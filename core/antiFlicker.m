function subjectTable = antiFlicker(subjectTable, nFrames)


% maintain list of subject's existence
subjectExists = logical(zeros(length(subjectTable),1));

% init list of subject's existence
for i = 1:length(subjectTable)
    subjectExists(i) = ~isempty(subjectTable{i}.frameInfo{1});
end

while (i<=nFrames)
    for j = 1:length(subjectTable)
        subject = subjectTable{j};
        subjectExistsNow = ~isempty(subject.frameInfo{i});
        
        if (subjectExistsNow && ~subjectExists(j))
            subjectLoc = subject.frameInfo{i}.blob.Centroid;
            % see if this is really just a new incarnation of an old
            % subject
            % go back a few frames
            kk = i-1:-1:max(1, i-8);
            for k = kk
                for m = 1:length(subjectTable)
                    % skip this subject, also skip if old subject didn't
                    % exist
                   
                    if m==j || isempty(subjectTable{m}.frameInfo{k})
                        continue;
                    end
                    
                    % see if previous subject is within a few pixels of the
                    % "new" one
                    oldSubjLoc = subjectTable{m}.frameInfo{k}.blob.Centroid;

                    dist = norm(subjectLoc-oldSubjLoc);
                    if (dist<30)
                        % merge these two subjects
                        fprintf('merge %d and %d\n', m, j);
                        
                        subjectTable = coalesceSubjects(subjectTable, nFrames, m, j);
                        
                        % recurse until no more coalescings are made
                        subjectTable = antiFlicker(subjectTable, nFrames);
                        return;
                        
                        
                    end
                    
                    
                end
            end
        end
        
        subjectExists(j) = subjectExistsNow;
        
    end
    i = i + 1;
end

end

function subjectTable = coalesceSubjects(subjectTable, nFrames, toIndex, fromIndex)

oldFrILen = length(subjectTable{toIndex}.frameInfo);
for i = 1:nFrames
    fromData = subjectTable{fromIndex}.frameInfo{i};
    if (~isempty(fromData))
        subjectTable{toIndex}.frameInfo{i} = fromData;
    end
end

newFrILen = length(subjectTable{toIndex}.frameInfo);
if (newFrILen~=oldFrILen)
    error('error!!');
end

subjectTable(fromIndex) = [];

for i = 1:length(subjectTable)
    subjectTable{i}.id = i;
end

end