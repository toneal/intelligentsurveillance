function subjectTable = flickerCheck(subjectTable)
% Checks subjectTable for subjects created in regions that wouldn't make
% sense. Assume subjects can only enter the video from the edge or openings
% like doorways and windows.

% testbed prohibited subject creation area
% call vidobj from global workspace
global vidobj;
marginSize = 50;
entryProhib = [marginSize, marginSize, (vidobj.width - 2*marginSize),...
    (vidobj.height - 2*marginSize)];

for ii = 1:length(subjectTable)
        currEnter = subjectTable{ii}.frameEntered;
        currExit = subjectTable{ii}.frameExited - 1; % want last known pos
        enterBBox = subjectTable{ii}.frameInfo{currEnter}.blob.BBox;
        enterCentroid = subjectTable{ii}.frameInfo{currEnter}.blob.Centroid;
        exitBBox = subjectTable{ii}.frameInfo{currExit}.blob.BBox;
        exitCentroid = subjectTable{ii}.frameInfo{currExit}.blob.Centroid;
        
        % Check if they entered or left in a place that doesn't make sense
        % Exempt first frame from this check.
        subjectTable{ii}.invalidEnter = test2DRectangleCollide(enterBBox, entryProhib) & currEnter ~= 1;
        subjectTable{ii}.invalidExit = test2DRectangleCollide(exitBBox, entryProhib) & currExit ~= vidobj.NumberOfFrames;
        
        % List out correlational candidates for invalid entries and exits.
        % Assume that every invalid entry has an invalid exit for perfect
        % blob detection. 
        %
        % TODO: Fix for imperfect blob detection.
        %
        corrCands = [];

        if subjectTable{ii}.invalidEnter
            % Look back at previous invalid exits to correlate
            % There's probably a way better way to do this. Whatever. Hacky
            % shit.
            for jj = 1:ii-1
                if isstruct(subjectTable{jj})
                    if subjectTable{jj}.invalidExit
                        oldExit = subjectTable{jj}.frameExited - 1;
                        oldExitCentroid = subjectTable{jj}.frameInfo{oldExit}.blob.Centroid;
                        centroidDist = norm(oldExitCentroid - enterCentroid);
                        corrCands = [corrCands centroidDist];
                    else
                        corrCands = [corrCands inf];
                    end
                else
                    corrCands = [corrCands inf];
                end
            end
        
        % Assume closest previous exit is the current subject
        closestNeighbor = find(corrCands == min(corrCands));
        
        % import subject information to closest neighbor
        % empties out invalid subjects
        subjectTable = mergeSubject(subjectTable, closestNeighbor, ii);
        end
        
end

    % cleanup table some
    invalidList = [];
    for ii = 1:length(subjectTable)
        invalidList(ii) = ~isstruct(subjectTable{ii});
    end
    subjectTable(find(invalidList)) = [];

end