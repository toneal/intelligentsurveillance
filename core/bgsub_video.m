function [bgblur, bgsubtract] = bgsub_video(vidname, framenums)
% input: vidname: video file path
% framenums: vector of frame indices to return
% returns bgblur: cell array of images
% bgsubtract: cell array of images
% each returned image is in the same location in the returned cell array as
% it is in the framenums vector
% each returned image is in unnormalized grayscale double format


% read in a file
vidobj = VideoReader(vidname);

% need number of frames and video resolution
nof = vidobj.NumberOfFrames;
vidHeight = vidobj.Height;
vidWidth = vidobj.Width;

% initialize frame for averaging video
bgframe = zeros(vidHeight,vidWidth,1,'double');

% read in a frame, convert to double, and average it into the bgframe
for kk = 1:nof
        inframe = rgb2gray(read(vidobj,kk));
        doubframe = double(inframe);
        bgframe = bgframe + doubframe./nof;
end

% now analyze all the frames specified by framenums
bgblur = {};
bgsubtract = {};
for i = 1:length(framenums)
    framenum = framenums(i);
    % pick a frame to subtract out bgframe
    testframe = rgb2gray(read(vidobj,framenum));
    doubtest = double(testframe);

    % the scaling is still weird. Some negative numbers appear?
    bgsubtractCurr = abs(doubtest - bgframe);

    gauss = fspecial('gaussian', [20 20], 2);
    bgblurCurr = imfilter(bgsubtractCurr, gauss);
    
    % append the results to the return arrays
    bgblur = [bgblur,{ bgblurCurr}];
    bgsubtract = [bgsubtract, {bgsubtractCurr}];
end


end

