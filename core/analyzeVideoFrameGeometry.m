
function returnValue = analyzeVideoFrameGeometry(frame, bgFrame)
% the input images should be in unnormalized double grayscale format
% this function returns a list of blob objects found in the input frame

if (size(frame, 3) > 1 | size(bgFrame, 3) > 1)
    error('must be grayscale');
end



bgSubbed = bgsub_frame(frame, bgFrame);
blobImg = blobify(bgSubbed);

global globalCache;


imagesc(blobImg);
colormap gray;

listBlobs = analyzeBlobImage(blobImg);

globalCache.listBlobs = listBlobs;

% only accept blobs with a bounding box bigger than a threshold dimension
bboxSizeThresh = 20;
listBlobsFinal = {};
for i = 1:length(listBlobs)
    blob = listBlobs{i};
    if (blob.BBox(3) > bboxSizeThresh && blob.BBox(4) > bboxSizeThresh)
        listBlobsFinal = [listBlobsFinal {blob}];
    end
end

globalCache.listBlobsFinal = listBlobsFinal;

returnValue = listBlobsFinal;

end