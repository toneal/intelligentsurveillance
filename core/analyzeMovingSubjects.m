function subjectTable =  analyzeMovingSubjects(viddata, bgFrame)

nFrames = size(viddata, 4);

% initialize table of subjects
subjectTable = {};

% if no provided bg frame, get the bg frame
if nargin == 1
    bgFrame = generateBGFrame(viddata, true);
end
    
frmLmt = nFrames;

% initialize everything for the first frame only
firstFrameBlobs = analyzeVideoFrameGeometry(double(rgb2gray(viddata(:, :, :, 1))), bgFrame);

subjIdCounter = 1;

% create initial list of subjects
for i = 1:length(firstFrameBlobs)
    currBlob = firstFrameBlobs{i};
    subjectFrameInfo = struct('blob', currBlob, 'blobIndex', i, 'subjID', subjIdCounter);

    subject = struct('id', subjIdCounter);
    frameInfo = cell(frmLmt,1);
    frameInfo{1} = subjectFrameInfo;

    subject.frameInfo = frameInfo;
    subjectTable{i} = subject;

    subjIdCounter = subjIdCounter + 1;
end

% subject table has been initialized, we are ready to loop

for i = 2:frmLmt
    fprintf('analyzing %d/%d\n', i, frmLmt);
    frame = double(rgb2gray(viddata(:, :, :, i)));
    prevFrame = double(rgb2gray(viddata(:, :, :, i-1)));
    
    [newSubjectFrameInfos numNewSubjectsFound] = correspondVideoFrames(prevFrame, i-1, frame, i, bgFrame, subjectTable, ...
        subjIdCounter);
    


    % increase the subj id counter if new subjects were found
    subjIdCounter = subjIdCounter + numNewSubjectsFound;

    if (numNewSubjectsFound > 0)
        fprintf('Subject id counter increased to %d\n', subjIdCounter);
    end

    % add the new subject infos
    for j = 1:length(newSubjectFrameInfos)
        newSubjectFrameInfo = newSubjectFrameInfos{j};

        foundInCurrentTable = false;

        for k = 1:length(subjectTable)
            subject = subjectTable{k};
            if (subject.id == newSubjectFrameInfo.subjID)
                %fprintf('Matched subj id\n');
                foundInCurrentTable = true;
                subjectTable{k}.frameInfo{i} = newSubjectFrameInfo;
            end
        end

        if (~foundInCurrentTable)
                fprintf('Adding subject %d to table \n', newSubjectFrameInfo.subjID);

                % create new subject object
                 subject = struct('id', newSubjectFrameInfo.subjID);
                 frameInfo = cell(frmLmt,1);
                 subject.frameInfo = frameInfo;

                % put the frame info in that object
                subject.frameInfo{i} = newSubjectFrameInfo;

                % add subject to table
                subjectTable{subject.id} = subject;
        end

    end

    
    
end

% subject table has been constructed
% run a bit more analysis, annotating the subject table with some
% additional, useful data

for i = 1:length(subjectTable)
    subject = subjectTable{i};
    frameEntered = -1;
    frameExited = -1;
    isIn = false;
    for j = 1:frmLmt
        frmDatum = subject.frameInfo{j};
        if (~isempty(frmDatum))
            isIn = true;
            if (frameEntered == -1)
                frameEntered = j;
            end
        else
            if (isIn)
                frameExited = j;
            end
            isIn = false;
        end
    end
    if (isIn)
        frameExited = frmLmt;
    end
    subject.frameEntered = frameEntered;
    subject.frameExited = frameExited;
    subjectTable{i} = subject;
end

% Check subjects for flicker due to blob detection issues
global globalCache;
globalCache.initSubjectTable = subjectTable;
subjectTable = antiFlicker(subjectTable, nFrames);



end