function bgFrame = generateBGFrame( video, convertToGrayscale )
% simply averages the input video (assumes RGB video)
% if convertToGrayscale is true, will output a grayscale double image,
% otherwise it 
% returns RGB background frame

avgVideo = double(video(:, :, :, 1))/256;

for i = 1:size(video, 4)
    vidFrame = double(video(:, :, :, i))/256;
    avgVideo = avgVideo + vidFrame;
end

avgVideo = avgVideo / size(video, 4);

if (convertToGrayscale)
    bgFrame = rgb2gray(avgVideo) * 256;
else
    bgFrame = uint8(avgVideo * 256);
end

end

