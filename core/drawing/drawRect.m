function [ imageWRect ] = drawRect(r,c,h,w,image, color )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
r = round(r);
c = round(c);
h = round(h);
w = round(w);

for rr=r:r+h
    image(rr,c,:) = color;    
end

for rr=r:r+h
    image(rr,c+w,:) = color;    
end

for cc=c:c+w
    image(r, cc,:) = color;
end

for cc=c:c+w
    image(r+h, cc,:) = color;
end
imageWRect = image;

end