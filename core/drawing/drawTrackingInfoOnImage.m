function img = drawTrackingInfoOnImage( img, analysisResults, frameIndex, roi )
%DRAWTRACKINGINFOONIMAGE Summary of this function goes here
%   Detailed explanation goes here
origImg = img;
subjectTable = analysisResults.subjects; % patching in current data structures
triggeredFrames = analysisResults.activatedTriggers;

for i = 1:length(subjectTable)
    subject = subjectTable{i};
    cFrameInfo = subject.frameInfo{frameIndex};
    if (~isempty(cFrameInfo))
        BBox = cFrameInfo.blob.BBox;
        centroid = cFrameInfo.blob.Centroid;
        [threatLabel, threatColor] = threatCheck(frameIndex, triggeredFrames, subject.id);
        img = drawRect(BBox(2), BBox(1), BBox(4), BBox(3), img, threatColor);
        img = drawTextOnImage(img, ['Obj ' num2str(i) ' - ' threatLabel], BBox(2) - 20, BBox(1), [0, 255 ,0], 12);
        img = drawTextOnImage(img, ['x: ' num2str(BBox(1)) ', y: ' num2str(BBox(2))], BBox(2) + BBox(4), BBox(1), [255 255 0], 12);
        img = drawRect(centroid(2), centroid(1), 2, 2, img, [0 0 255]);
        img = drawRect(roi(1), roi(2), roi(3), roi(4), img, [0 0 255]);
        
    end
end

img = img(1:size(origImg,1), 1:size(origImg, 2), :);

end

function [objTextAppend, objTextColor] = threatCheck(index, triggeredFrames, currID)

    inRestrict = 0; % initialize
    % test if subject is inside restricted region
    for ii = 1:length(triggeredFrames)
        if (index >= triggeredFrames{ii}.frame) &&...
                (currID == triggeredFrames{ii}.subjectId) &&...
                (index <= triggeredFrames{ii}.eventEnd)
            inRestrict = 1;
        else
        end
    end
        
    if (inRestrict)
        objTextAppend = 'Alert';
        objTextColor = [255 0 0];
    else
        objTextAppend = 'OK';
        objTextColor = [0 255 0];
    end

end