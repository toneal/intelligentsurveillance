function out = drawTextOnImage(Img, text, r, c , color, size)
% inputs: an rgb image Img
% text: text string to draw
% r, c: where to draw string
% color: 1x3 rgb vector in range [0, 255]
% size: font size in points

% outputs: a copy of the image with the specified text drawn

textInserter = vision.TextInserter(text,'Color', color, 'FontSize', size, 'Location', [c, r]);
out = step(textInserter, Img);

end