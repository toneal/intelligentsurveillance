function [ HOGVis ] = extractBlobHog(frame, blob)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

subject = imcrop(frame,blob.BBox);
[featureVector, HOGVis] = extractHOGFeatures(subject);
figure;
imagesc(subject); hold on;
plot(HOGVis);

end

