function retVal = generateBase64Image(img)

imwrite(img, 'temp.png', 'png');

fh = fopen('temp.png', 'rb');
bts = fread(fh);
fclose(fh);
encoder = org.apache.commons.codec.binary.Base64;
base64string = char(encoder.encode(bts))';
retVal = base64string;

end