function returnVal = findSubjectIDByBlobIndex( subjectTable, frameIndex, blobIndex )

for i = 1:length(subjectTable)
    subject = subjectTable{i};
    subjectFrameInfo = subject.frameInfo{frameIndex};
    if (~isempty(subjectFrameInfo))
        if (subjectFrameInfo.blobIndex == blobIndex)
            returnVal = subject.id;
            return;
        end
    end
end

frameIndex
blobIndex
error('no subject id found for that combo of frame index and blob index');

end

