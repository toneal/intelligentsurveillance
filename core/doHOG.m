function op = doHOG( img, cellSizeX, cellSizeY )
%DOHOG Summary of this function goes here
%   Detailed explanation goes here
if (size(img, 3)>1)
    error('only handles grayscale');
end

gradImgMag = imfilter(img, [0 -1 0; -1 0 1; 0 1 0]);
[gradImgX gradImgY] = gradient(img);

[rs cs] = size(img);
cellCountX = floor(size(img, 2)/cellSizeX) + 1;
cellCountY = floor(size(img, 1)/cellSizeY) + 1;

% initialize all the cell structs
cellData = cell(cellCountY, cellCountX);
for r = 1:size(cellData, 1)
    for c = 1:size(cellData, 2)
        cellData{r, c} = struct('sumVotes', [0;0], 'votes', zeros(2,0));
    end
end

% compute the actual data now
for r = 1:rs
    for c = 1:cs
        cellX = floor((c-1)/cellSizeX) + 1;
        cellY = floor((r-1)/cellSizeY) + 1;
        cellDatum = cellData{cellY, cellX};
        weightedVote = gradImgMag(r, c) .* [gradImgX(r, c); gradImgY(r,c)];
        cellDatum.sumVotes = cellDatum.sumVotes + weightedVote;
        cellDatum.votes = [cellDatum.votes, weightedVote];
        cellData{cellY, cellX} = cellDatum;
    end
end

op = struct('HOGData', {cellData}, 'cellSizeX', cellSizeX,'cellSizeY', cellSizeY, 'imageSizeX', size(img, 2), 'imageSizeY', size(img, 1));
end

