function rv = analyzeBlobImage(img)
B = vision.BlobAnalysis;
B.AreaOutputPort = false;
B.MaximumCount = 5;
[Centroid, BBox] = step(B, img);   % Acquiring coordinates, height, width of BBox
BBox = double(BBox); % make everything doubles for simplicity
Centroid = double(Centroid);

rv = {};
for i = 1:size(BBox,1)
    BBoxcurr = BBox(i, :);
    Centroidcurr = Centroid(i, :);
    st = struct('BBox', BBoxcurr, 'Centroid', Centroidcurr);
    rv(i) = {st};
end


end