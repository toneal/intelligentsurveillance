function blob = blobify(input)
% input: an image

%handle input color images
if (size(input, 3)==3)
    input = grayify(input);
    fprintf('grayify triggered in blobify\n');
end

% output a logical image (thresholded blob detect)
% old scale factor was 27
hAutothreshold = vision.Autothresholder('ThresholdScaleFactor', 27);
hClosing = vision.MorphologicalClose('Neighborhood', strel('square',5));
thresh = step(hAutothreshold,input);
blob = step(hClosing,thresh);

end