function [ out ] = grayify( in )
%GRAYIFY Summary of this function goes here
% converts any image or video to unnormalized grayscale

% handle color images
if (strcmp(class(in),'uint8') && size(in, 3)==3)
    in = double(rgb2gray(in));
end

out = in;
end

