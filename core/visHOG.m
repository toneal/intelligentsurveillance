function  visHOG( hogData )

clf;
cells = hogData.HOGData;

for r = 1:size(cells, 1)
    for c = 1:size(cells, 2)
        currCell = cells{r, c};
        if (norm(currCell.sumVotes) ==0)
            normedSum = [0;0];
        else
            normedSum = currCell.sumVotes/norm(currCell.sumVotes);
        end
        % twist the normed sum by 90 degrees to show the shape of the image
        normedSum = [-normedSum(2);normedSum(1)];
        
        % make a vector to plot the normed sum
        
        cntr = [(c-1/2) * hogData.cellSizeX; (r-1/2) * hogData.cellSizeY];
        p0 = cntr;
        p1 = cntr + normedSum * hogData.cellSizeX/2;
        lin = [p0 p1];
        plot(lin(1, :), lin(2,:), 'b-');
        hold on;
        
        
    end
end

axis ij;



end

