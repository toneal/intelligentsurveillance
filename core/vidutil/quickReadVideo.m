function vd = quickReadVideo(fileName)
vidobj = VideoReader(fileName);
vd = read(vidobj);
end