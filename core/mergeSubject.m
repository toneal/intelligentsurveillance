function subjectTable = mergeSubject(subjectTable, mergeeInd, mergerInd)

mergerInfoExists = [];
for ii = 1:length(subjectTable{mergerInd}.frameInfo)
    mergerInfoExists = ~isempty(subjectTable{mergerInd}.frameInfo{ii});
    if mergerInfoExists
        subjectTable{mergeeInd}.frameInfo{ii} = subjectTable{mergerInd}.frameInfo{ii};
    end
end

subjectTable{mergerInd} = [];



end