function drawPoly2D(poly, styl)

polyC = [poly, poly(:,1)];
plot(polyC(1,:), polyC(2,:), styl);
end