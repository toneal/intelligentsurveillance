function drawCameraCone(G_WC, xFOVDegrees, yFOVDegrees)

greenRayL = 26;
rayC = [linspace(0,0);linspace(0,0);linspace(0,greenRayL);ones(1, 100)];
rayW = zeros(4, 100);
for i = 1:100
    rayW(:, i) = G_WC * rayC(:, i);
end
plot3(rayW(1,:), rayW(2,:), rayW(3,:), 'g');
hold on;
plot3(G_WC(1, 4), G_WC(2, 4), G_WC(3,4),'rx');
taox = tand(xFOVDegrees);
taoy = tand(yFOVDegrees);
zdist = 3;

completePoly = @(polyPts)([polyPts, polyPts(:, 1)]);

tris = {};
tri1 = [(zdist * [0, taox, -taox;0, taoy, taoy; 0,1,1;]);[1,1,1]];
tri2 = [(zdist * [0, -taox, -taox;0, taoy, -taoy; 0,1,1]); [1,1,1]];
tri3 = [(zdist * [0, -taox, taox;0, -taoy, -taoy; 0,1,1]); [1,1,1]];
tri4 = [(zdist * [0, taox, taox;0, -taoy, taoy; 0,1,1]); [1,1,1]];
tris = {tri1, tri2, tri3, tri4};
for i = 1:length(tris)
    tris{i} = completePoly(tris{i});
    tris{i} = transformPoints(tris{i}, G_WC);
    finTri = tris{i};
    plot3(finTri(1,:),finTri(2,:),finTri(3,:),'b-');

end


end