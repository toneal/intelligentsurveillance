% need the three camera points 
% calculate the vars needed for the camera points
l = 36
w = 32
h = 12
hs = 3
ws = 2
r = sqrt(l^2/4 + w^2/4)
v = sqrt((h-hs)^2 + r^2)
Psi = [...
   400 ,    0,   320;...
     0,  -400,   240;...
     0,     0,     1];
costheta = r/v
sintheta = sqrt(1-costheta^2)
g = sqrt(h^2 + l^2/4 + w^2/4)
p = (v + hs + g)/2;
A = sqrt(p*(p-v)*(p-hs)*(p-g))
y3 = 2*A/g
z3 = sqrt(v^2 + y3^2)


r_camera = [0;r*sintheta;r*costheta]
v_camera = [0;0;v]
g_camera = [0;-y3;z3]

r_world = [-w/2; l/2; 0];
v_world = [-w/2; l/2; hs-h];
G_WC_test = calculateCornerCameraTransform([w;0;h], [r_camera, v_camera], [r_world, v_world])

G_WCs = {
    calculateCornerCameraTransform([w;0;h], [r_camera, v_camera], [[-w/2; l/2;0],[-w/2;l/2;hs-h]]),...
    calculateCornerCameraTransform([0;0;h], [r_camera, v_camera], [[w/2; l/2;0],[w/2;l/2;hs-h]]),...
    calculateCornerCameraTransform([0;l;h], [r_camera, v_camera], [[w/2; -l/2;0],[w/2;-l/2;hs-h]]),...
    calculateCornerCameraTransform([w;l;h], [r_camera, v_camera], [[-w/2; -l/2;0],[-w/2;-l/2;hs-h]])

};

clf;
% draw the camera FOVS and labels
for i = 1:length(G_WCs)
    G_WC = G_WCs{i};
    T_WC = G_WC(1:3,4);
    drawCameraCone(G_WC, 30, 30);
    text(T_WC(1), T_WC(2), T_WC(3), ['    Camera ' num2str(i)]);
end
xlabel('x');
ylabel('y');
zlabel('z');

% draw some walls
drawWall([0;0;0],[0;1;0],w,h);
drawWall([w;0;0],[-1;0;0],l,h);
drawWall([w;l;0],[0;-1;0],w,h);
drawWall([0;l;0],[1;0;0],l,h);

% generate points for the stand
standPolys = {
    generateWallPoly([w/2-ws/2;l/2-ws/2;0],[0;1;0],ws,hs);
    generateWallPoly([w/2-ws/2;l/2+ws/2;0],[1;0;0],ws,hs);
    generateWallPoly([w/2+ws/2;l/2+ws/2;0],[0;-1;0],ws,hs);
    generateWallPoly([w/2+ws/2;l/2-ws/2;0],[-1;0;0],ws,hs);
};

% draw the stand
for i = 1:length(standPolys)
    drawPoly(standPolys{i},'k-');
end

% draw a little point on the middle of the stand
plot3(w/2,l/2,hs,'rx');

% now create the views from all 4 cameras
for i = 1:length(G_WCs)
    G_WC = G_WCs{i};
    figure(i+1);
    hold off;
    standPolysC = {};
    for j = 1:length(standPolys)
        standPolysC{j} = transformPoints(standPolys{j},inv(G_WC));
    end
    % draw the stand
    for k = 1:length(standPolys)
        % transform to image units
        standPoly2D = projectPoints(standPolysC{k}, Psi);
        drawPoly2D(standPoly2D, 'k-');
        hold on;
    end
        title(['Camera ' num2str(i) ' view of stand']);
        xlabel('x');
        ylabel('y');
        zlabel('z');

end

for i = 1:length(G_WCs)
    G_WC = G_WCs{i}
    R_WC = G_WC(1:3, 1:3)
    dR = det(R_WC)
end



