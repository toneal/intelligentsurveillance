function drawWall(T_LL, N, w, h)
horizUnitVec = cross(N, [0;0;1]);
horizUnitVec = horizUnitVec/norm(horizUnitVec);
vertUnitVec =[0;0;1];
wallLL = T_LL;
wallLR = T_LL + w*horizUnitVec;
wallUR = wallLR + h *vertUnitVec;
wallUL = wallUR - w*horizUnitVec;
pts = [wallLL,wallLR,wallUR,wallUL,wallLL];
plot3(pts(1,:),pts(2,:),pts(3,:),'k-');
end