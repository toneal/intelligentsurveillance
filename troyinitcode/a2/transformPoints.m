function rv = transformPoints(ptsA, G_BA)
[rptsa, cptsa] = size(ptsA);
G_BA
if (rptsa==3)
    ptsA = [ptsA;ones(1, cptsa)];
end
ptsA
ptsB = G_BA * ptsA;
[r c] = size(ptsB);
[r c]
for i = 1:c
    ptsB(:, i) = ptsB(:, i)/ptsB(4,i);
end
rv = ptsB;
end