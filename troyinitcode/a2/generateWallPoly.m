function rv = generateWallPoly(T_LL, N, w, h)
horizUnitVec = cross(N, [0;0;1]);
horizUnitVec = horizUnitVec/norm(horizUnitVec);
vertUnitVec =[0;0;1];
wallLL = T_LL;
wallLR = T_LL + w*horizUnitVec;
wallUR = wallLR + h *vertUnitVec;
wallUL = wallUR - w*horizUnitVec;
pts = [wallLL,wallLR,wallUR,wallUL];
rv  = pts;
end