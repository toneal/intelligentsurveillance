function rv = projectPoints(ptsC, Psi)
[r c] = size(ptsC);
for i = 1:c
        ptsC(:,i) = ptsC(:,i)./ptsC(4,i);
end
ptsC =ptsC(1:3,:);
ptsIm = Psi*ptsC;
[r c] = size(ptsIm);
for i = 1:c
    ptsIm(:,i) = ptsIm(:,i)./ptsIm(3,i);
end
rv = ptsIm;
end