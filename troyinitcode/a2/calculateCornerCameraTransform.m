function G_WC = calculateCornerCameraTransform(T_WC, ptsC, ptsW)
% x coords of camera points must be zero for this to work, embarassingly
% enough
BM = [ptsC(2,1),ptsC(3,1),0,0,0,0;...
    0,0,ptsC(2,1),ptsC(3,1),0,0;...
    0,0,0,0,ptsC(2,1),ptsC(3,1);...
    ptsC(2,2),ptsC(3,2),0,0,0,0;...
    0,0,ptsC(2,2),ptsC(3,2),0,0;...
    0,0,0,0,ptsC(2,2),ptsC(3,2)];
LV = [ptsW(:, 1);ptsW(:, 2)];
RV = BM\LV;
Rcol2 = RV(1:2:end);
Rcol3 = RV(2:2:end);
Rcol1 = cross(Rcol2, Rcol3);
R = [Rcol1, Rcol2, Rcol3];

G_WC = [R, T_WC; 0,0,0,1];
end