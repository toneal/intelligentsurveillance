function drawPoly(poly, styl)

polyC = [poly, poly(:,1)];
plot3(polyC(1,:), polyC(2,:), polyC(3,:), styl);
end