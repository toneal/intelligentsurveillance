

cR = [0.433,-0.250,-0.866;0.058,0.967,-.250;.9,0.058,.433];
cPos = [5;8;-4];
horizFV = 45;
vertFV = 60;
sinfvx = sind(horizFV);
sinfvy = sind(vertFV);
cosfvx = cosd(horizFV);
cosfvy = cosd(vertFV);
wx = 5;
wy = wx * cosfvx/cosfvy;

clf;
plot3(cLPos(1),cLPos(2),cLPos(3),'rx');
hold on;

tri1 = [[0;0;0],[0;wy * sinfvy;wy*cosfvy],[wx*sinfvx;0;wx*cosfvx]];
tri2 = [[0;0;0],[0;wy * sinfvy;wy*cosfvy],[-wx*sinfvx;0;wx*cosfvx]];
tri3 = [[0;0;0],[0;-wy * sinfvy;wy*cosfvy],[-wx*sinfvx;0;wx*cosfvx]];
tri4 = [[0;0;0],[0;-wy * sinfvy;wy*cosfvy],[wx*sinfvx;0;wx*cosfvx]];
tri1 = [tri1,tri1(:,1)];
tri2 = [tri2,tri2(:,1)];
tri3 = [tri3,tri3(:,1)];
tri4 = [tri4,tri4(:,1)];


plot3(tri1(1,:), tri1(2, :), tri1(3, :));
plot3(tri2(1,:), tri2(2, :), tri2(3, :));
plot3(tri3(1,:), tri3(2, :), tri3(3, :));
plot3(tri4(1,:), tri4(2, :), tri4(3, :));
xlabel('x');
ylabel('y');
zlabel('z');


% draw walls
[yy zz] = meshgrid(linspace(-5,10,10), linspace(0, 10, 10));
xx = 4*ones(10,10);
surf(xx,yy,zz);
[xx zz] = meshgrid(linspace(0,4,10), linspace(0, 10, 10));
yy = -5*ones(10,10);
surf(xx,yy,zz);
colormap gray;


