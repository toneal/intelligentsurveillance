function plotFilterTrajectories(traj)
 
 figure(7);
 signl = traj(:, 1);
 
 dt = 1/30;
 tt = 0:dt:dt*(size(signl, 1)-1);
 clf;
 

 subplotposs = [ { [1,1] }, { [1, 2] }, { [2, 1] }, { [2, 2] } ];
 titles = { 'x', 'y', 'X Dim', 'Y Dim' };
 titleVel = { 'x vel', 'y vel', 'X vel', 'Y vel' };
 
 figure(7);
 clf;
 for c = 1:4
     signl = traj(:, c);
     [filtsignlpos vel] = customKalmanFilter(signl, dt);
     subplotp = subplotposs{c};
     subplot(2, 2, c);
     
     plot(tt, filtsignlpos, 'r');
     hold on;
     plot(tt, signl, 'b');
     title(titles{c});
     xlabel('t (s)');
     ylabel('px');
     legend('filtered', 'raw');
 end
annotation('textbox', [0 0.9 1 0.1], ...
    'String', 'Kalman-filtered Signals', ...
    'EdgeColor', 'none', ...
    'HorizontalAlignment', 'center')
 
 figure(8);
 clf;
  for c = 1:4
     signl = traj(:, c);
     [filtsignlpos vel] = customKalmanFilter(signl, dt);
     subplotp = subplotposs{c};
     subplot(2, 2, c);
     
     plot(tt, vel, 'r');
     title(titleVel{c});
     xlabel('t (s)');
     ylabel('px/s');
   
  end
 annotation('textbox', [0 0.9 1 0.1], ...
    'String', 'Kalman-filtered Velocity Signals', ...
    'EdgeColor', 'none', ...
    'HorizontalAlignment', 'center')

end
 