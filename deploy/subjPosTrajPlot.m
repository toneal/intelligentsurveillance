function outPlot = subjPosTrajPlot(subjID, subjList)

    currSubj = subjList{subjID}
    
    length = 1 + currSubj.frameExited - currSubj.frameEntered;
    currTraj = zeros(length, 4);
    currPos = zeros(length, 2);

    % Position data retrieval
    for ii = currSubj.frameEntered:currSubj.frameExited-1
 
        posFr = currSubj.frameInfo{ii}.blob.Centroid;
        currPos(ii, :) = posFr;
    end
    
    % Trajectory data retrieval
    for jj = currSubj.frameEntered:currSubj.frameExited-1
        bboxFr = currSubj.frameInfo{jj}.blob.BBox;
        currTraj(jj, :) = bboxFr;
    end
    
    dt = 1/30;
    tt = currSubj.frameEntered * dt:dt:dt*(currSubj.frameExited-1);
    ndxRelevant = currSubj.frameEntered:currSubj.frameExited - 1;
    
    figure(1);
    clf;
    annotation('textbox', [0 0.9 1 0.1], ...
               'String', 'Position of Subject', ...
               'EdgeColor', 'none', ...
               'HorizontalAlignment', 'center')
        subplot(2,1,1)
            title('X Position')
            plot(tt,currPos(ndxRelevant,1));
            xlabel('t(s)');
            ylabel('pixels');
        
        subplot(2,1,2)
            title('Y Position')
            plot(tt,currPos(ndxRelevant,2));
            xlabel('t(s)');
            ylabel('pixels');    
        
    % We've already written the trajectory filtering scripts
    %plotFilterTrajectories(currTraj);
    %kalmanFiltTraj(currTraj);
    
    outPlot = {};
%save processing time, only output the first plot for now
    for ll = 1:1
        currFile = strcat('../outputs/subj',num2str(subjID),'Pos0',num2str(ll),'.png');
        saveas(ll,currFile);
        outPlot{ll} = imread(currFile);
    end
%     
%     for ll = 3:8
%         currFile = strcat('../outputs/subj',num2str(subjID),'Traj0',num2str(ll),'.png')
%         saveas(ll,currFile);
%         outPlot{ll} = imread(currFile);
%     end
    
end