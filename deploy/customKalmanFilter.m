
 
 function [pos vel] = customKalmanFilter(signl, dt)
 
 varVelocityInitial = 9;
 
 N = length(signl);
 
 % our state is position and velocity
 
 s = struct( ...
        'x', [signl(1); 0],  ...  % state vector initial estimate
        'A', [1 dt;0 1],... % state transition matrix
        'Q', zeros(2, 2), ... % process noise covariance
        'P', [0 0; 0 varVelocityInitial], ... % initial estimate covariance matrix,
        'B', [0 0; 0 0], ... % control matrix,
        'H', eye(2), ... % observation matrix,
        'u', [0;0],... % control vector
        'R', [0.01 0; 0 100] ... % measurement noise covariance
 );

pos = zeros(N, 1);
vel = zeros(N, 1);
for t=1:N
   measCurr = signl(t);
   s(end).z = measCurr;
   pos(t) = s(end).x(1);
   vel(t) = s(end).x(2);
   s(end+1)=kalmanf(s(end)); % perform a Kalman filter iteration
   %outputFromKalman = kalmanf(s(end))
   if (size(s(end).x, 2) == 2)
       error('error, bro!!1');
   end
   
end

 end


 
 