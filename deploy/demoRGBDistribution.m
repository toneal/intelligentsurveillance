img = imread('../Datasets/HOGIso.png');
figure(1);
clf;
imagesc(img);
grimg = grayify(img);
blobbed = grimg > 1;
imagesc(blobbed);
colormap gray;
rgbPts = zeros(sum(sum(blobbed)), 3);
i = 1;
for r = 1:size(img, 1)
    for c = 1:size(img, 2)
        if (blobbed(r,c))
            rgbPts(i, :) = double(img(r, c, :));
            i = i + 1;
        end
    end
end

redChan = rgbPts(:, 1);
greenChan = rgbPts(:, 2);
blueChan = rgbPts(:, 3);

redMean = mean(redChan);
redVar = var(redChan);
greenMean = mean(greenChan);
greenVar = var(greenChan);
blueMean = mean(blueChan);
blueVar = var(blueChan);

redStdv = sqrt(redVar);
blueStdv = sqrt(blueVar);
greenStdv = sqrt(greenVar);

% plot 3 gaussian distributions
xx = linspace(0, 256);
redGauss = normpdf(xx, redMean, redStdv);
blueGauss = normpdf(xx, blueMean, blueStdv);
greenGauss = normpdf(xx, greenMean, greenStdv);
figure(2);
plot(xx, redGauss, 'r');
hold on;
plot(xx, blueGauss, 'b');
plot(xx, greenGauss, 'g');
axis([0 256 0 0.012]);

% print the covariance matrix
covMatrix = cov(rgbPts);

figure(3);
clf;
plot3(redChan, greenChan, blueChan, 'b.');
hold on;
xlabel('R');
ylabel('G');
zlabel('B');
plot3(redMean, greenMean, blueMean, 'rx');


    
