function filterTraj = trajFilter(traj, type, length)
% Applies one of three filters:
%     'average, 'gaussian', and 'kalman'
    switch type
        case 'average'
            filterTraj = imfilter(traj, ...
                  fspecial('average', [1 length]), 'replicate');
        case 'gaussian'
            filterTraj = imfilter(traj, ...
                  fspecial('gaussian', [1 length], length/3), 'replicate');
        %case kalman goes here
    end
end