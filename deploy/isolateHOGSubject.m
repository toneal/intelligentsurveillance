clear all;
clip = quickReadVideo('Datasets/clip0.avi');
frm = clip(:, :, :, end/2);
bgFrame = generateBGFrame(clip, true);
blobs = analyzeVideoFrame(grayify(frm), bgFrame);
bgSubbed = bgsub_frame(grayify(frm), bgFrame);
bgSubbed_color = bgsub_frame(frm, generateBGFrame(clip, false));
clf;imagesc(bgSubbed);colormap gray;
blobImg = blobify(bgSubbed);
imagesc(blobImg);
bbox = blobs{1}.BBox;
outp = uint8(zeros(bbox(4), bbox(3), 3));
for r = 1:size(outp,1)
    for c = 1:size(outp, 2)
        imptx = c + bbox(1);
        impty = r + bbox(2);
        if (blobImg(impty, imptx))
            outp(r, c, :) = frm(impty, imptx, :);
        end
    end
end

imagesc(outp);
imwrite(outp, '../Datasets/HOGIso.png', 'png');