function img = generateTrackingVideoFrame(srcFrame, bgSubtractBlurFrame, restricRegion)
% src frame should be a RGB image read directly from the video file
% bgSubtractBlurFrame should be a grayscale image result from the bg
% subtraction process

% extract the blob of the person
blobtest = persondetect(bgSubtractBlurFrame);

% get the relevant frame in the video
inframe = rgb2gray(srcFrame);
doubframe = double(inframe);

%normalize doubframe
doubframe = doubframe./256;
% create RGB image
doubframe = repmat(doubframe, [1 1 3]);

% get the bounding box of the blob
[bbox centroid] = calculateBBoxesFromBinaryBlobImage(blobtest);
img = doubframe;
% draw the bounding box on the image
img = drawRect(bbox(2), bbox(1), bbox(4), bbox(3), img, [1 1 0]);
% draw the centroid on the image
img = drawRect(centroid(2), centroid(1), 2, 2, img, [0 0 1]);
% draw text
%img = drawTextOnImage(imageWithBBox, 'Hello', bbox(2), bbox(1), [0 255 0], 18);

% test if subject is inside restricted region
inRestrict = test2DRectangleCollide(restricRegion, bbox);
if (inRestrict)
    objTextAppend = 'Alert';
    objTextColor = [255 0 0];
else
    objTextAppend = 'OK';
    objTextColor = [0 255 0];
end

img = drawTextOnImage(img, ['Obj0 - ' objTextAppend], bbox(2)- 24, bbox(1), objTextColor, 12);
img = drawTextOnImage(img, ['x: ' num2str(bbox(1)) ', y: ' num2str(bbox(2))], bbox(2) + bbox(4), bbox(1), [255 255 0], 12);

% draw restricted region
img = drawRect(restricRegion(2), restricRegion(1), restricRegion(4), restricRegion(3), img, [1 0 0]);


end