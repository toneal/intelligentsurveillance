function plotFilterTrajectories(traj)

% Making a data structure to house the different modes
filterTraj = {};

filter = zeros(size(traj));
% Apply kinematic filters on each column
    % forward difference
    for ii = 1:4
        filter(:,ii) = trajKinematics(traj(:,ii)','forward',1/30);
    end
    filterTraj{1} = filter;
    
    % backwards difference
    for ii = 1:4
        filter(:,ii) = trajKinematics(traj(:,ii)','backward',1/30);
    end
    filterTraj{2} = filter;
    
    % central difference
    for ii = 1:4
        filter(:,ii) = trajKinematics(traj(:,ii)','central',1/30);
    end
    filterTraj{3} = filter;
    
    % acceleration
    for ii = 1:4
        filter(:,ii) = trajKinematics(traj(:,ii)','acceleration',1/30);
    end
    filterTraj{4} = filter;
    
% Apply other filters on each column
    % average
    for ii = 1:4
        filter(:,ii) = trajFilter(traj(:,ii)','average',20);
    end
    filterTraj{5} = filter;
    
    % gaussian
    for ii = 1:4
        filter(:,ii) = trajFilter(traj(:,ii)','gaussian',20);
    end
    filterTraj{6} = filter;
    
%     % Kalman filter
%     for ii = 1:4
%         filter(:,ii) = trajFilter(traj(:,ii)','kalman',1/30);
%     end
%     filterTraj{7} = filter;


% Why doesn't Matlab support dictionaries like Python?
characteristicList = {'X Loc', 'Y Loc', 'X Dim', 'Y Dim'};
colorList = {'r', 'g', 'b'};
dt = 1/30;
tt = 0:dt:dt*(size(traj,1)-1);

% plot each characteristic first
for jj = 1:4 % jj is organized by characteristic
    figure(2)
        subplot(2,2,jj)
        title(characteristicList(jj));
        plot(tt,traj(:,jj));
        annotation('textbox', [0 0.9 1 0.1], ...
   'String', 'Raw Signal', ...
   'EdgeColor', 'none', ...
   'HorizontalAlignment', 'center')
    xlabel('t(s)');
    ylabel('pixels');
end

% plot each kinematic filter next
for jj = 1:4
    figure(3)
        subplot(2,2,jj)
        title(characteristicList(jj));
        annotation('textbox', [0 0.9 1 0.1], ...
   'String', 'Kinematic velocity', ...
   'EdgeColor', 'none', ...
   'HorizontalAlignment', 'center')
        hold on
        for kk = 1:3 % kk organized by type of filter
            plot(tt,filterTraj{kk}(:,jj),colorList{kk});
            xlabel('t(s)');
            ylabel('pixels/s');
        end
        if(jj == 2)
            legend('Forward','Backward','Central');
        end
        
end

% plot acceleration filter next
for jj = 1:4
    figure(4)
        subplot(2,2,jj)
        title(characteristicList(jj));
         annotation('textbox', [0 0.9 1 0.1], ...
   'String', 'Kinematic Acceleration', ...
   'EdgeColor', 'none', ...
   'HorizontalAlignment', 'center')
        hold on
        plot(tt,filterTraj{4}(:,jj));
        xlabel('t(s)');
        ylabel('pixels/s^2');
end

% plot average filter next
for jj = 1:4
    figure(5)
        subplot(2,2,jj)
        title(characteristicList(jj));
        annotation('textbox', [0 0.9 1 0.1], ...
   'String', 'Average filter', ...
   'EdgeColor', 'none', ...
   'HorizontalAlignment', 'center')
        hold on
        plot(tt,filterTraj{5}(:,jj));
         xlabel('t(s)');
        ylabel('pixels');
        
end

% plot gaussian filter next
for jj = 1:4
    figure(6)
        subplot(2,2,jj)
        title(characteristicList(jj));
        annotation('textbox', [0 0.9 1 0.1], ...
   'String', 'Gaussian filter', ...
   'EdgeColor', 'none', ...
   'HorizontalAlignment', 'center')
        hold on
        plot(tt,filterTraj{6}(:,jj));
        xlabel('t(s)');
        ylabel('pixels');
end

end