function kineTraj = trajKinematics(traj, type, dt)
% Applies one of four kinematic filters:
%     'forward, 'backward', 'central', and 'acceleration'
    switch type
        case 'forward'
            kineTraj = imfilter(traj, [ 0 -1 1]/dt, 'replicate');
        case 'backward'
            kineTraj = imfilter(traj, [ -1 1 0]/dt, 'replicate');
        case 'central'
            kineTraj = imfilter(traj, [ -1 0 1]/2/dt, 'replicate');
        case 'acceleration'
            kineTraj = imfilter(traj, [ 1 -2 1]/dt^2, 'replicate');
    end
end