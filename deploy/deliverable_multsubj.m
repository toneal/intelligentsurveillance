% read in a file
clear all;
vidFilePath = '../Datasets/multipleObj1.avi';

vidobj = VideoReader(vidFilePath);
viddata = read(vidobj);
nFrames = vidobj.NumberOfFrames;

% create a region of restriction
restricRegion = [10, 300, 100, 100];

% get subject table
subjectTable = analyzeMovingSubjects(viddata);


% now subject table is complete, time to draw images into an output video
outVid = viddata(:, :, :, 1:frmLmt);
for i = 1:frmLmt
    img = viddata(:, :, :, i);
    imgRaw = img;
    img = drawTrackingInfoOnImage(img, subjectTable, i);
    outVid(:, : ,:, i) = img;
    fprintf('Rendered frame %d/%d\n', i, frmLmt);
end

quickWriteVideo(outVid, '../outputs/trackingMulti1.avi');



