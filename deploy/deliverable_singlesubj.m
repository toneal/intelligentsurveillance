% read in a file
vidobj = VideoReader('Datasets/clip0.avi');
viddata = read(vidobj);
nFrames = vidobj.NumberOfFrames;

% create a region of restriction
restricRegion = [10, 300, 100, 100];

[bgblur, bgsubtract] = bgsub('Datasets/clip0.avi', 1:nFrames);

frmLmt = 264;

outvid = double(viddata);
outvid(:,:,:,:) = 0;
for i = 1:frmLmt
    img = generateTrackingVideoFrame(viddata(:,:,:,i), bgblur{i}, restricRegion);
    fprintf('Frame %d/%d rendered\n', i, frmLmt);
    outvid(:, :, :, i) = img;
end

figure(1);
clf;
imagesc(img);

vidwr = VideoWriter('outputs/tracking0.avi');
open(vidwr);
writeVideo(vidwr, outvid);
close(vidwr);
