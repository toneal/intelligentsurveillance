vidFilePath = '../Datasets/multipleObj1.avi';

vidobj = VideoReader(vidFilePath);
viddata = read(vidobj);
nFrames = vidobj.NumberOfFrames;

frame1 = double(rgb2gray(viddata(:, :, :, 1)));
bgSubbed = bgsub_frame(frame1, generateBGFrame(viddata, true));
imagesc(bgSubbed);

imwrite(viddata(:, :, :, 1), '../outputs/origFrame_demo_0.png');

imwrite(bgSubbed/256, '../outputs/bgsubtract_demo_0.png');

blobImg = blobify(bgSubbed);

imagesc(blobImg);

imwrite(blobImg, '../outputs/blobbed_demo_0.png');

colormap gray;