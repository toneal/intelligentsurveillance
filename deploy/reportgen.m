function reportgen(date, cameraID, cameraLoc, startTime, endTime, alert,...
                                                    trigger, subjects)
    % Provide a date, cameraID number, location, start time, end time, and
    % cell arrays for alerts, rules triggered, and subject information.
    
    reportname = strcat(num2str(cameraID), 'report', num2str(date),...
                                                              '.html');
    report = fopen(reportname, 'w');
    
    fprintf(report, strcat('Camera ID:\t\t\t', cameraID, '\n'));
    fprintf(report, strcat('Camera Location:\t', cameraLoc, '\n'));
    fprintf(report, strcat('Start Time:\t\t\t', startTime, '\n'));
    fprintf(report, strcat('End Time:\t\t\t', endTime, '\n'));
    
    fprintf(report, '\n');
    
    fprintf(report, 'Alert(s): \n');
    for ii = 1:length(alert)
        fprintf(report, strcat('\t', alert{ii}, '\n'));
    end
    
    fprintf(report, '\n');
    
    fprintf(report, 'Details: \n');
    for jj = 1:length(trigger)
        fprintf(report, strcat('\tTimestamp:\t\t\t\t', num2str(trigger{jj}{1}), '\n'));
        
        % Just gonna list this as the snapshot location until I think of a
        % better way to implement it.
        fprintf(report, strcat('\tTrigger Snapshot:\t\t', num2str(trigger{jj}{2}), '\n'));
        fprintf(report, strcat('\tSubject ID Snapshot:\t', num2str(trigger{jj}{3}), '\n'));
        
        fprintf(report, strcat('\tIndicator:\t\t\t\t', num2str(trigger{jj}{4}), '\n'));
        fprintf(report, strcat('\tAdditional Metadata:\t', num2str(trigger{jj}{5}), '\n\n'));
    end
    
    fprintf(report, '\n');
    
    fprintf(report, 'Subject(s): \n');
    for kk = 1:length(subjects)
        fprintf(report, strcat('\tKinematic Data:\t\t\t', num2str(subjects{kk}{1}), '\n'));
        fprintf(report, strcat('\tAdditional Metadata:\t', num2str(subjects{kk}{2}), '\n'));
        fprintf(report, strcat('\tEvent Timeline:\t\t\t', num2str(subjects{kk}{1}), '\n\n'));
    end
    
    fclose(report);
    
end