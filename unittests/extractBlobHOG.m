function [ HOGVis ] = extractBlobHOG(frame, bbox)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

subject = imcrop(frame,bbox);
[featureVector, HOGVis] = extractHOGFeatures(subject);
figure;
imagesc(subject); hold on;
plot(HOGVis);

end

