clear all;
img = imread('../Datasets/HOGIso.png');
img = grayify(img);

figure(1);
clf;
imagesc(img);

op = doHOG(img, 3, 3);
figure(2);
visHOG(op);